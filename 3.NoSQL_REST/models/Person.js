const db = require('mongoose');

const Schema = db.Schema;
const Model = db.model;

const PersonSchema = new Schema({
    name: String,
    surname: String
});

const Person = Model("Person", PersonSchema);

module.exports = { Person };