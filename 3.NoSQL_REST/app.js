const express = require(`express`),
      app = express(),
      mongoose = require(`mongoose`),
      parser = require(`body-parser`),
      override  = require('method-override');

const { Person } = require(`./models/Person`);
const SeedDB = require(`./seed`);

const host = `127.0.0.1`;
const port = 3000;
const dbUrl = `mongodb://localhost:27017/akademia`;

mongoose
  .connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(SeedDB())
  .catch(err => console.log(err));

app.set(`view engine`, `pug`);
app.use(parser.json())
app.use(parser.urlencoded({ extended: true }))
app.use(override('_method'));

app.get(`/`, (_, res) => {
  res.redirect(`/index`)
})

app.get(`/index`, (_, res) => {
  res.render(`index`, { title: `Index Page`, message: `Index!` });
});

app.get('/people', async (_, res) => {
  try {
    const list = await Person.find();
    res.render('people/index', { title: 'lista uczniów', list: list });
  } catch (err) {
    console.err(err);
  }
});

app.get(`/people/new`, (_, res) => {
  res.render(`people/new`);
});

app.post(`/people`, async (req, res) => {
  try {
    const person = new Person(req.body);
    await person.save();
    res.redirect(`/people`);
  } catch (err) {
    console.error(err);
  }
});

app.get(`/people/:id`, async (req, res) => {
  try {
    const id = req.params.id;
    const person = await Person.findById(id);
    res.render('people/edit', { _id: person._id, name: person.name, surname: person.surname });
  } catch (err){
    console.error(err);
  }
});

app.get(`/people/delete/:id`, async (req, res) => {
  try {
    const id = req.params.id;
    const person = await Person.findById(id);
    res.render('people/delete', { _id: person._id});
  } catch (err){
    console.error(err);
  }
});

app.delete(`/people/delete/:id`, async (req, res) => {
  try {
    const id = req.params.id;
    const person = await Person.findByIdAndDelete(id);
    await person.save();
    res.redirect(`/people`);
  } catch (err) {
    console.error(err);
  }
});

app.put(`/people/:id`, async (req, res) => {
  try {
    const id = req.params.id;
    const person = await Person.findById(id);
    person.set(req.body);
    await person.save();
    res.redirect(`/people`);
  } catch (err) {
    console.error(err);
  }
});


app.get(`*`, (_, res) => {
  res.send(`Not Found`);
});

app.listen(port, host, () => {
  console.log(`Running on http://${host}:${port}/`);
});
