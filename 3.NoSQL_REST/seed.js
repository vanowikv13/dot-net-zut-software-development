const { Person } = require("./models/Person");

const data = [
    { name: "Szymon", surname: "Dąbrowski" },
    { name: "Grzegorz", surname: "Dziugieł" },
    { name: "Krzysiek", surname: "Staciwa" }
];

const Seed = async () => {
    try {
        await Person.deleteMany({}).exec();
        data.forEach((item) => {
            Person.create(item, (_, obj) => {
                console.log(obj);
            });
        });
    } catch (err) {
        console.log(err);
    }
}

module.exports = Seed;